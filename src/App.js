import {useState} from "react"
import Player from "./components/Player";
import Song from "./components/Song";
import data from "./data";
import Library from "./components/Library";
import Nav from "./components/Nav";

import './styles/app.scss';


function App() {
  // eerste is een variable [songs], tweede is een functie [setSongs].
  // Bij de eerste sla je het op en met de tweede wijzig je de opgeslagen gegevens.
  const [songs, setSongs] = useState(data());
  const [currentSong, setCurrentSong] = useState(songs[0]);
  const [isPlaying, setIsPlaying] = useState(false);
  const [libraryStatus, setLibraryStatus] = useState(false);

  // componenten
  return (
    <div className={`App ${libraryStatus ? 'library-active' : ''}`}> 
      <Nav libraryStatus={libraryStatus} setLibraryStatus={setLibraryStatus}/>
      <Song currentSong={currentSong}/>
      <Player currentSong={currentSong} isPlaying={isPlaying} setIsPlaying={setIsPlaying}/>
      <Library songs={songs} setCurrentSong={setCurrentSong} libraryStatus={libraryStatus} />
    </div>
  );
}

export default App;