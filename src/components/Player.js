import {useState, useRef} from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlay, faPause, faStepForward, faStepBackward } from "@fortawesome/free-solid-svg-icons";

const Player = ({currentSong, isPlaying, setIsPlaying}) => {

    const audioRef = useRef(null)
    
    const playSongHandler = () => {
        if(isPlaying) {
            audioRef.current.pause();
            setIsPlaying(!isPlaying);
        } else {
            audioRef.current.play();
            setIsPlaying(!isPlaying);
        }
    };

    const [songInfo, setSongInfo] = useState({
        currentTime: null,
        duration: null,
    })

    const timeUpdateHandler = (e) => {
        // console.log(e);
        const currentTime = e.target.currentTime;
        const duration = e.target.duration;
        console.log(currentTime);
        setSongInfo({...songInfo, currentTime: currentTime, duration: duration});
    }

    const getTime = (time) => {
        return(
            Math.floor(time / 60) + ":" + ("0" + Math.floor(time % 60)).slice(-2)
        );
    }

    const dragHandler = (e) => {
        console.log(e);
        audioRef.current.currentTime = e.target.value;
        setSongInfo({...songInfo, currentTime: e.target.value});
    }

    const autoPlayHandler = () => {
        if(isPlaying){
            audioRef.current.play();
        }
    }
    
    // const skipBackSongHandler = () => {

    // };

    // const skipForwardSongHandler = () => {

    // };


    return(
        <div className="player">
            <div className="time-control">
                <p>{getTime(songInfo.currentTime)}</p>
                <input onChange={dragHandler} min="0" max={songInfo.duration} value={songInfo.currentTime}type="range" />
                <p>{getTime(songInfo.duration)}</p>
            </div>
            <div className="player-control">
            <FontAwesomeIcon className="skip-back"  size="2x" icon={faStepBackward}  />
            <FontAwesomeIcon onClick={playSongHandler} className="play" size="2x" icon={isPlaying ? faPause : faPlay} />
            <FontAwesomeIcon className="skip-forward" size="2x" icon={faStepForward} />
            </div>
            <audio 
            onTimeUpdate={timeUpdateHandler}
            onLoadedData={autoPlayHandler}
            onLoadedMetadata={timeUpdateHandler}
            ref={audioRef} 
            src={currentSong.audio}
            ></audio>
        </div>
    );
}

export default Player;